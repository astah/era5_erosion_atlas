# Rain Erosion Atlas

This repository contains python scripts that generate the rain erosion atlas layers of the publication: [Rain erosion atlas for wind turbine blades based on ERA5 and NORA3 for Scandinavia](https://www.sciencedirect.com/science/article/pii/S2590123024002639). The scripts are setup to read the data as they are stored on the computational clusters of the authors Universities. 

## Output layers

The output layers contain: Annual average rainfall [mm], mean wind speed at 150 m [m/s], annual acummulated impinged rainfall [m], and erosion onset time [years].

## Authors

Ásta Hannesdóttir (astah@dtu.dk) and Stephan T. Kral (stephan.kral@uib.no)


## License

[MIT](LICENSE)

## How to cite
If you use this data or code for academic research, you are encouraged to cite our paper:
### Text format:
Ásta Hannesdóttir, Stephan T. Kral, Joachim Reuder, Charlotte Bay Hasager,
Rain erosion atlas for wind turbine blades based on ERA5 and NORA3 for Scandinavia,
Results in Engineering,
Volume 22,
2024,
102010,
ISSN 2590-1230,
https://doi.org/10.1016/j.rineng.2024.102010.
(https://www.sciencedirect.com/science/article/pii/S2590123024002639)

### BibTeX format:
```
@article{hannesdottir2024,
title = {Rain erosion atlas for wind turbine blades based on ERA5 and NORA3 for Scandinavia},
journal = {Results in Engineering},
volume = {22},
pages = {102010},
year = {2024},
issn = {2590-1230},
doi = {https://doi.org/10.1016/j.rineng.2024.102010},
url = {https://www.sciencedirect.com/science/article/pii/S2590123024002639},
author = {\'{A}sta Hannesd\'ottir and Stephan T. Kral and Joachim Reuder and Charlotte Bay Hasager},
keywords = {Rain erosion atlas, Wind turbine blade coating, Precipitation and wind, Empirical damage model, Impingement to end of incubation, Offshore conditions}
}
```