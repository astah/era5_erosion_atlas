# %%
import xarray as xr
import matplotlib.pyplot as plt
import cartopy.crs as ccrs          # import projections
import cartopy.feature as cf        # import features
import matplotlib.gridspec as gridspec
import numpy as np
#import os


# %% ERA5 data
ds = xr.open_dataset('data/atlas_layers_1.nc')
i_lat_central = round(len(ds.latitude)/2)
i_lon_central = round(len(ds.longitude)/2)
c_lon = float(ds.longitude[i_lon_central])
c_lat = float(ds.latitude[i_lat_central]) 

# create land sea mask ERA5
era5_lsm_file='data/ERA5_land_sea_mask.nc'
# era5_land_file='data/ERA5_some_t2m_land_data.nc'
# if os.path.isfile(era5_lsm_file):
LSM=xr.open_dataset(era5_lsm_file)
# else:
#     LSM = xr.open_dataset(era5_land_file)
lsme = LSM.lsm[0,:,:]

lsme = np.where(lsme>=0.5, 1, lsme) # 0 is for pure offshore, 1 pure land. 0.5 is a good compromise to match the coastline

#     LSM["land_sea_mask"]=['latitude','longitude'], np.where(lsme!=0, 1, lsme)
#     LSM.drop('t2m')
#     LSM.to_netcdf(era5_lsm_file)
lsme=lsme[-1::-1,:] # latitudes come in reverse order
ds["land_sea_mask"]=['latitude','longitude'],lsme.data
# %% NORA3 data
dn = xr.open_dataset('data/NORA3_statistics.nc')
dnxmax=646

# Land sea mask
lsm_file='data/NORA3_land_sea_mask.nc'
LSM =xr.open_dataset(lsm_file)

dn["land_sea_mask"]=(['y', 'x'],  LSM.land_area_fraction.data[0,0,:,:])

# dn = xr.open_dataset('data/NORA3_LEEatlas.nc')
#%%
# for jj in range(0,652):
# #    plt.plot(dn.annual_accumulated_rain[jj,:])
#     plt.plot(dn.annual_accumulated_rain[:,jj])
# #%%    
# for yy in range(0,892):
#     plt.plot(dn.annual_accumulated_rain[yy,:])
# %% Annual average rainfall and wind speed and Mean wind speed at 150 m ============================================
# Variable for the plot:
r_var = dn.annual_accumulated_rain[:,0:dnxmax]
r = ds.annual_rainfall*1000
r_max = 2500
r_min = r_var.min().values
w = ds.wsp_150m
w_var = dn.mean_ws[:,0:dnxmax]
w_max = max(w.max(), w_var.max()) 
w_min = min(w.min(), w_var.min()) 
plotProj = ccrs.Orthographic(central_longitude=c_lon, central_latitude=c_lat)

fig = plt.figure(figsize=(10,9))
# Define the gridspec
gs = gridspec.GridSpec(2, 3, width_ratios=[1, 1, 0.04])
# Create the subplots
ax1 = fig.add_subplot(gs[0, 0], projection=plotProj)
ax2 = fig.add_subplot(gs[0, 1], projection=plotProj)
ax3 = fig.add_subplot(gs[1, 0], projection=plotProj)
ax4 = fig.add_subplot(gs[1, 1], projection=plotProj)
#-----------------------------------------------------------
cax1 = r.plot.pcolormesh(ax=ax1, transform=ccrs.PlateCarree(), cmap='viridis',
                         add_colorbar=False, vmax=r_max, vmin=r_min)
ax1.coastlines(linewidth=0.8)
ax1.set_title('')
gl = ax1.gridlines(draw_labels=True, linewidth=0.4)
gl.right_labels = False
xlim, ylim = ax1.get_xlim(), ax1.get_ylim()
#-----------------------------------------------------------
ax2.pcolormesh(dn.lon[:,0:dnxmax], dn.lat[:,0:dnxmax], r_var, transform=ccrs.PlateCarree(), cmap='viridis', vmax=r_max, vmin=r_min)
ax2.set_xlim(xlim)
ax2.set_ylim(ylim)
ax2.coastlines(linewidth=0.8)
gl = ax2.gridlines(draw_labels=True, linewidth=0.4)
gl.right_labels = False
#-----------------------------------------------------------
cax3 = w.plot.pcolormesh(ax=ax3, transform=ccrs.PlateCarree(), cmap='plasma',
                         add_colorbar=False, vmin=w_min, vmax=w_max)
ax3.coastlines(linewidth=0.8)
ax3.set_title('')
gl = ax3.gridlines(draw_labels=True, linewidth=0.4)
gl.right_labels = False
#------------------------------------------------------------
ax4.pcolormesh(dn.lon[:,0:dnxmax], dn.lat[:,0:dnxmax], w_var, transform=ccrs.PlateCarree(),
                       cmap='plasma', vmin=w_min, vmax=w_max)
ax4.set_xlim(xlim)
ax4.set_ylim(ylim)
ax4.coastlines(linewidth=0.8)
gl = ax4.gridlines(draw_labels=True, linewidth=0.4)
gl.right_labels = False

# Add labels
labels = ["a)", "b)", "c)", "d)"]
for ax, label in zip([ax1, ax2, ax3, ax4], labels):
    ax.text(0.05, 0.95, label, transform=ax.transAxes, 
            verticalalignment='top', horizontalalignment='left',
            fontsize=14)

# Add colorbars
cbax1 = fig.add_subplot(gs[0, 2])
cbax2 = fig.add_subplot(gs[1, 2])
fig.colorbar(cax1, cax=cbax1, label='annual average rainfall [mm]', extend='max')
fig.colorbar(cax3, cax=cbax2, label='mean wind speed at 150m [m/s]')

plt.tight_layout()
plt.savefig('plots/wind_rain_layers.png', dpi=300)


# %% Annual average impinged rain and erosion onset time ============================================================
# Variable for the plot:
ir_var = dn.annual_accumulated_impinged_rain[:,0:dnxmax]
sea_mask=-1*dn.land_sea_mask[:,0:dnxmax]+1              #invert data (1 is original for land we want it for sea)
sea_mask=np.where(sea_mask==0, np.nan, sea_mask)
ir_var_offshore = ir_var*sea_mask
ir = ds.annual_impinged_rainfall
sea_mask_e=-1*ds.land_sea_mask+1              #invert data (1 is original for land we want it for sea)
sea_mask_e=np.where(sea_mask_e==0, np.nan, sea_mask_e)
ir_offshore=ir*sea_mask_e

ir_max = 40 #max(ir_var.max(), ir.max())
ir_min = min(r_var.min(), ir.min())
e = ds.erosion_time
e_offshore=e*sea_mask_e
e_var = 1/dn.annual_accumulated_damage_inc[:,0:dnxmax]
e_var_offshore = e_var*sea_mask
e_max = 25
e_min = min(e.min(), e_var.min())
e_max_offshore = 7.5
e_min_offshore = min(e_offshore.min(),e_var_offshore.min())

fig = plt.figure(figsize=(10,9))
# Create the subplots
ax1 = fig.add_subplot(gs[0, 0], projection=plotProj)
ax2 = fig.add_subplot(gs[0, 1], projection=plotProj)
ax3 = fig.add_subplot(gs[1, 0], projection=plotProj)
ax4 = fig.add_subplot(gs[1, 1], projection=plotProj)
#-----------------------------------------------------------
cax1 = ir.plot.pcolormesh(ax=ax1, transform=ccrs.PlateCarree(), cmap='viridis',
                         add_colorbar=False, vmax=ir_max, vmin=ir_min)
ax1.coastlines(linewidth=0.8)
ax1.set_title('')
gl = ax1.gridlines(draw_labels=True, linewidth=0.4)
gl.right_labels = False
xlim, ylim = ax1.get_xlim(), ax1.get_ylim()
#-----------------------------------------------------------
ax2.pcolormesh(dn.lon[:,0:dnxmax], dn.lat[:,0:dnxmax], ir_var, transform=ccrs.PlateCarree(), 
               cmap='viridis', vmax=ir_max, vmin=ir_min)
ax2.set_xlim(xlim)
ax2.set_ylim(ylim)
ax2.coastlines(linewidth=0.8)
gl = ax2.gridlines(draw_labels=True, linewidth=0.4)
gl.right_labels = False
#-----------------------------------------------------------
cax3 = e.plot.pcolormesh(ax=ax3, transform=ccrs.PlateCarree(), cmap='plasma_r',
                         add_colorbar=False, vmin=e_min, vmax=e_max)
ax3.coastlines(linewidth=0.8)
ax3.set_title('')
gl = ax3.gridlines(draw_labels=True, linewidth=0.4)
gl.right_labels = False
#------------------------------------------------------------
ax4.pcolormesh(dn.lon[:,0:dnxmax], dn.lat[:,0:dnxmax], e_var, transform=ccrs.PlateCarree(),
                       cmap='plasma_r', vmin=e_min, vmax=e_max)
ax4.set_xlim(xlim)
ax4.set_ylim(ylim)
ax4.coastlines(linewidth=0.8)
gl = ax4.gridlines(draw_labels=True, linewidth=0.4)
gl.right_labels = False

# Add labels
labels = ["a)", "b)", "c)", "d)"]
for ax, label in zip([ax1, ax2, ax3, ax4], labels):
    ax.text(0.05, 0.95, label, transform=ax.transAxes, 
            verticalalignment='top', horizontalalignment='left',
            fontsize=14)

# Add colorbars
cbax1 = fig.add_subplot(gs[0, 2])
cbax2 = fig.add_subplot(gs[1, 2])
fig.colorbar(cax1, cax=cbax1, label='annual accumulated impinged rainfall [m]', extend='max')
fig.colorbar(cax3, cax=cbax2, label='erosion onset time [years]', extend='max')

plt.tight_layout()
plt.savefig('plots/impingement_layers.png', dpi=300)
# %% NORA3/ERA5 offshore erosion onset
e_min_offshore=e_var_offshore.min()
e_max_offshore=7.5
fig = plt.figure(figsize=(10,4.5))
gs = gridspec.GridSpec(1, 3, width_ratios=[1, 1, 0.04])

# Create the subplots
ax4 = fig.add_subplot(gs[0, 1], projection=plotProj)
ax3 = fig.add_subplot(gs[0, 0], projection=plotProj)

cbax4=ax4.pcolormesh(dn.lon[:,0:dnxmax], dn.lat[:,0:dnxmax], e_var_offshore, transform=ccrs.PlateCarree(),
                       cmap='plasma_r', vmin=e_min_offshore, vmax=e_max_offshore)
ax4.set_xlim(xlim)
ax4.set_ylim(ylim)
ax4.coastlines(linewidth=0.8)
gl = ax4.gridlines(draw_labels=True, linewidth=0.4)
gl.right_labels = False

e_offshore.plot.pcolormesh(ax=ax3, transform=ccrs.PlateCarree(), cmap='plasma_r',
                         add_colorbar=False, vmin=e_min_offshore, vmax=e_max_offshore)
ax3.coastlines(linewidth=0.8)
ax3.set_title('')
gl = ax3.gridlines(draw_labels=True, linewidth=0.4)
gl.right_labels = False

# Add labels
labels = ["a)","b)"]
for ax, label in zip([ax3,ax4], labels):
    ax.text(0.05, 0.95, label, transform=ax.transAxes, 
            verticalalignment='top', horizontalalignment='left',
            fontsize=14)

# Add colorbars
#cbax1 = fig.add_subplot(gs[0, 2])
cbax2 = fig.add_subplot(gs[0, 2])
fig.colorbar(cbax4, cax=cbax2, label='erosion onset time [years]', extend='max')

plt.tight_layout()
plt.savefig('plots/offshore_damage_onset.png', dpi=300)

# %% A map of the area with all the points of interest and domain outlines

lats = [61.33052996, 60.3865774, 56.78323489, 56.31460465,
        55.60049638, 55.1320584, 54.02584054, 56.58196945,
        54.98442932, 54.55561815, 55.03484532, 54.61739797, 54.98071577]

lons = [2.69265503, 5.32885313, 4.89964479, 7.64475032,
        7.56816907, 7.20172205, 6.5953546, 11.20749075,
        11.12510747, 11.5310804, 12.93590264, 12.64171453, 14.7344281]
n = np.arange(len(lats)) +1
# lons_shift = [x+0.01 for x in lons]

lat_meas = [59.3164059, 56.70589548]
lon_meas = [4.86959211,  8.2338674]

''' To get those indexes you find: dn.lon.argmin(dim=['x','y']),
dn.lon.argmax(dim=['x','y']) etc...'''
nora_lon_domain = [dn.lon[0,0].values, dn.lon[891,0].values, dn.lon[891,651].values, dn.lon[0,651].values, dn.lon[0,0].values]
nora_lat_domain = [dn.lat[0,0].values, dn.lat[891,0].values, dn.lat[891,651].values, dn.lat[0,651].values, dn.lat[0,0].values]

myProj = ccrs.Orthographic(central_longitude=c_lon, central_latitude=c_lat)
myProj._threshold = myProj._threshold*10.

fig = plt.figure(figsize=(10,5))
ax = fig.add_subplot(121, projection=myProj)
# ax.set_extent([-10, 40, 47, 77], crs=ccrs.PlateCarree())
ax.add_feature(cf.LAND)
ax.add_feature(cf.OCEAN)
ax.add_feature(cf.COASTLINE, linewidth=0.3)
ax.add_feature(cf.BORDERS, linestyle=':', linewidth=0.3)
# ax.add_feature(cf.LAKES, alpha=0.5)
# ax.add_feature(cf.RIVERS)
gl = ax.gridlines(draw_labels=True, linewidth=0.7)
gl.right_labels = False
ax.plot([ds.longitude.values[0], ds.longitude.values[0], ds.longitude.values[-1], ds.longitude.values[-1], ds.longitude.values[0]],
        [ds.latitude.values[0], ds.latitude.values[-1], ds.latitude.values[-1], ds.latitude.values[0], ds.latitude.values[0]],
        transform=ccrs.PlateCarree(), label='ERA5')
ax.plot(nora_lon_domain, nora_lat_domain,
        transform=ccrs.PlateCarree(), label='NORA3')
ax.text(0.03, 0.97, 'a)', transform=ax.transAxes, 
            verticalalignment='top', horizontalalignment='left', fontsize=14)
ax.legend(loc='lower right')

ax2 = fig.add_subplot(122, projection=plotProj)
ax2.set_extent([2, 18.5, 53.4, 62], crs=ccrs.PlateCarree())
ax2.add_feature(cf.LAND)
ax2.add_feature(cf.OCEAN)
ax2.add_feature(cf.COASTLINE, linewidth=0.2)
ax2.add_feature(cf.BORDERS, linestyle=':', linewidth=0.2)
gl = ax2.gridlines(draw_labels=True, linewidth=0.7)
gl.right_labels = False
ax2.scatter(lons,lats,transform=ccrs.PlateCarree(), marker='2', color='green',
             s=90, linewidth=2.0, label='wind farm')
ax2.scatter(lon_meas, lat_meas, transform=ccrs.PlateCarree(), marker='o', color='red',
             s=30, linewidth=1, label='weather station')
for i, txt in enumerate(['A', 'B']):
    ax2.annotate(txt, (lon_meas[i]+0.3, lat_meas[i]-0.08), transform=ccrs.PlateCarree(), fontsize=10)
for i, txt in enumerate(n):
    ax2.annotate(txt, (lons[i]+0.3, lats[i]-0.08), transform=ccrs.PlateCarree(), fontsize=10)
ax2.text(0.03, 0.97, 'b)', transform=ax2.transAxes, 
            verticalalignment='top', horizontalalignment='left', fontsize=14)
ax2.legend()
plt.savefig('plots/domain_map.png', dpi=300)

# %%
