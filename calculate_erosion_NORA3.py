# %%
import xarray as xr
import netCDF4 as nc
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from dask.diagnostics import ProgressBar
import scipy
import warnings
import time
import os
#
# start option (to avoid running the entire script)
start_at=5
use_local=0 #local or remotely stored nc input files 

# size of subdomain to reduce memory 
dy=223 # 1,2,4,223,446,892
dx=163 # 1,2,4,163,326,652

# %% Define input parameters
dt = 3600   # The time step of the input data in seconds
file_path = '../NORA3_data_wind_precip_1992-2021_subdomain/'
remote_file_path = 'https://thredds.met.no/thredds/dodsC/nora3/'

lat_slice = slice(52,73)
lon_slice = slice(2,31)
#t=pd.date_range(start='2015-01-01T00:00:00.000000000',end='2015-12-31T23:00:00.000000000',freq='H')
ty=[pd.date_range(start='2015-01-01T00:00:00.000000000',end='2015-12-31T23:00:00.000000000',freq='H'),
    pd.date_range(start='2016-01-01T00:00:00.000000000',end='2016-12-31T23:00:00.000000000',freq='H'),
    pd.date_range(start='2017-01-01T00:00:00.000000000',end='2017-12-31T23:00:00.000000000',freq='H'),
    pd.date_range(start='2018-01-01T00:00:00.000000000',end='2018-12-31T23:00:00.000000000',freq='H'),
    pd.date_range(start='2019-01-01T00:00:00.000000000',end='2019-12-31T23:00:00.000000000',freq='H')
    ]
tl=np.sum(np.fromiter((len(t) for t in ty),int))
# t=pd.date_range(start='2015-01-01T00:00:00.000000000',end='2019-12-31T23:00:00.000000000',freq='H')
#t=pd.date_range(start='2015-06-13T06:00:00.000000000',end='2015-06-13T07:00:00.000000000',freq='H')
# time_slice = slice(t[0],t[-1])
height_slice = slice(100,250)
N3_xgrid=range(1489361,3442362,3000)
N3_ygrid=range(-649477,2023524,3000)
x_slice = slice(N3_xgrid.start-1,N3_xgrid.stop)
xl = int((x_slice.stop-x_slice.start)/N3_xgrid.step)+1
#xl=652#x_slice.stop-x_slice.start
y_slice = slice(N3_ygrid.start-1,N3_ygrid.stop) # corresponding to y-indeces(207,1098)
yl=int((y_slice.stop-y_slice.start)/N3_ygrid.step)+1
hub_height = 150#[100,250] #250 interpolation required (linear)
dv={'atmosphere_as_single_layer' ,
    'height1','height3','height4','height_above_msl','pressure0','top_of_atmosphere',
    'atmosphere_level_of_free_convection',
    'convective_cloud_area_fraction',
      #'graupelfall_amount',
      #'graupelfall_amount_acc',
    'forecast_reference_time',
     #'hail_diagnostic',
    'lifting_condensation_level',
    'lwe_thickness_of_atmosphere_mass_content_of_water_vapor',
    'projection_lambert',
      #'snowfall_amount',
      #'snowfall_amount_acc',
      # 'rainfall_amount',
    'air_temperature_0m',
	'surface_geopotential',
	'liquid_water_content_of_surface_snow',
	'downward_northward_momentum_flux_in_air',
	'downward_eastward_momentum_flux_in_air',
	'integral_of_toa_net_downward_shortwave_flux_wrt_time',
	'integral_of_surface_net_downward_shortwave_flux_wrt_time',
	'integral_of_toa_outgoing_longwave_flux_wrt_time',
	'integral_of_surface_net_downward_longwave_flux_wrt_time',
	'integral_of_surface_downward_latent_heat_evaporation_flux_wrt_time',
	'integral_of_surface_downward_latent_heat_sublimation_flux_wrt_time',
	'water_evaporation_amount',
	'surface_snow_sublimation_amount_acc',
	'integral_of_surface_downward_sensible_heat_flux_wrt_time',
	'integral_of_surface_downwelling_shortwave_flux_in_air_wrt_time',
	'integral_of_surface_downwelling_longwave_flux_in_air_wrt_time',
	'air_temperature_2m',
	'relative_humidity_2m',
	'specific_humidity_2m',
	'x_wind_10m',
	'y_wind_10m',
	'cloud_area_fraction',
	'x_wind_gust_10m',
	'y_wind_gust_10m',
	'air_temperature_max',
	'air_temperature_min',
	'convective_cloud_area_fraction',
	'high_type_cloud_area_fraction',
	'medium_type_cloud_area_fraction',
	'low_type_cloud_area_fraction',
	'atmosphere_boundary_layer_thickness',
	'air_pressure_at_sea_level',
	'lwe_thickness_of_atmosphere_mass_content_of_water_vapor',
	'surface_air_pressure',
	'lifting_condensation_level',
	'atmosphere_level_of_free_convection',
	'atmosphere_level_of_neutral_buoyancy',
	'wind_direction',
	'wind_speed',
    'x_wind_pl', 'y_wind_pl',
    'air_temperature_pl',
    'cloud_area_fraction_pl',
    'geopotential_pl',
    'relative_humidity_pl',
    'upward_air_velocity_pl', 
	#'snowfall_amount_acc',
    }
dvr=dv.copy()
dvr.add('x_wind_z')
dvr.add('y_wind_z')
dv2={'p0','ap','b','projection_lambert','forecast_reference_time','specific_humidity_ml','turbulent_kinetic_energy_ml',
     'cloud_area_fraction_ml','toa_net_downward_shortwave_flux','surface_downwelling_shortwave_flux_in_air',
     'toa_outgoing_longwave_flux','surface_downwelling_longwave_flux_in_air','atmosphere_boundary_layer_thickness',
     'pressure_departure','surface_air_pressure','air_temperature_ml','surface_geopotential',
     'x_wind_ml','y_wind_ml','air_pressure_at_sea_level','precipitation_amount_acc',
     'hybrid','top_of_atmosphere','height_above_msl'}

dims_4d = ('time','hubheight','y','x')
dims_3d = ('time','y','x')
<<<<<<< HEAD
dims_2d = ('y','x')
#
=======
dims_2d = ('y', 'x')

# download land sea mask NORA3
lsm_file='data/NORA3_land_sea_mask.nc'
if os.path.isfile(lsm_file)==False:
    lsm = xr.open_dataset(remote_file_path + '2019/01/01/00/fc2019010100_009.nc',
          drop_variables=dv2).sel(y=y_slice, x=x_slice)
    lsm.to_netcdf(lsm_file)
    

>>>>>>> 88d16047a4a3c30a91ec0669dbb8f68154e2826e
# %% Functions
def droplet_diameter(rain_rate, percentile=50):
    """Best formula for droplet diameter for 50th percentile of the droplet distribution.
    Args:
        rain_rate (array, float): rain rate in [mm/h]
        percentile (float, int): percentile of the droplet size in range 0-100%
    Returns: 
        d50: droplet diameter in [mm]
    """
    a_par, p, n = 1.3, 0.232, 2.25
    a = a_par*rain_rate**p
    d50 = (-np.log(percentile/100))**(1/n)*a
    return d50 

def terminal_velocity(droplet_diameter):
    """A fitted function to the Best data with droplet terminal velocity as function of droplet diameter
    Args:
        droplet_diameter (array_type): Droplet diameter in mm
    Returns:
        v_term (array_type): The terminal velocity of the droplets in m/s
    """
    dd = droplet_diameter
    v_term = 0.0481 * dd**3 - 0.8037 * dd**2 + 4.621 * dd
    return v_term

def tip_speed(wsp, turbine):
    """A function that calculates tip speed from wind speed.
    Args:
        wsp (array_like): Hub height wind speed input
        turbine (string): 'NREL_5MW', 'DTU_10MW', or 'IEA_15MW'
    Returns:
        ts_inter (array_like): The tip speed of the selected wind turbine
    """
    ts_file = 'tip_speed/' + turbine + '.csv'
    ts_data = pd.read_csv(ts_file)
    ts_inter = np.interp(wsp, ts_data['wsp'],
                         ts_data['tip_speed'], left=0, right=0)
    return ts_inter
    
def impinged_rain(wsp, rainfall, dt, turbine, c=2.85e22, m = 10.5):
    """A function that calculates impinged rain on a blade tip and corresponding damage increments
    Args:
        wsp (arra_type): Wind speed at hub height [m/s]
        rainfall (array_type): The rainfall in [m]
        dt (int or float): The time step of the wsp and rainfall data [s]
        turbine (string):'NREL_5MW', 'DTU_10MW', or 'IEA_15MW'
        c (float, optional): Rain erosion test power-law constant. Defaults to 2.85e22.
        m (float, optional): Rain erosion test power-law constant. Defaults to 10.5.
    Returns:
        rainfall_b (array_like): The impinged rainfall on the blade tip [m] 
        damage_inc (array_like): Tha damage increments. When they sum up to one damage begins to occur
    """
    v_tip = tip_speed(wsp, turbine)
    # Rain rate in SI units
    rain_rate_si = rainfall/dt
    # Rain rate in [mm/h]
    rain_rate = rain_rate_si*1000*3600
    droplet_d = droplet_diameter(rain_rate)
    v_term = terminal_velocity(droplet_d)
    # Water content in a unit volume of air [-]
    water_content = xr.where(rain_rate_si > 0, rain_rate_si/v_term, 0)
    # Rain rate on the blade tip [m/s] (SI unit)
    rain_rate_b = water_content * v_tip # [m/s]
    # Impinged rainfall on the blade tip [m]
    rainfall_b = rain_rate_b*dt
    # Average impact velocity of the rain drops relative to the blade (averaging out vertical velocity)
    v_rel = np.sqrt(wsp**2 + v_tip**2)
    # Modelled impingement to reach damage in RET 
    # Impingement to damge as function of the relative velocity
    impingement = xr.where(v_rel>0, c*v_rel**(-m), np.inf)
    # Damage increments
    damage_inc = rainfall_b/impingement
    return rainfall_b, damage_inc

# try:
#     # %% Read in the ERA5 data
#     file_path = '/groups/reanalyses/era5/app/'
#     lat_slice = slice(53,72)
#     lon_slice = slice(0,32)
#     time_slice = slice('2010-01-01T00:00:00.000000000','2019-12-31T23:00:00.000000000')
#     hub_height = 150
    
#     ds_pre = xr.open_zarr(file_path + 'era5_precip.zarr')
#     ds_ws = xr.open_zarr(file_path + 'era5.zarr')
#     # %% Make a new dataset for the output
#     ds = xr.Dataset()
#     ds['ws'] = ds_ws.WS.sel(height=hub_height, 
#                             latitude=lat_slice, 
#                             longitude=lon_slice, 
#                             time=time_slice)
#     ds['tp'] = ds_pre.tp.sel(latitude=lat_slice, 
#                              longitude=lon_slice, 
#                              time=time_slice)
#     ds['ptype'] = ds_pre.ptype.sel(latitude=lat_slice, 
#                              longitude=lon_slice, 
#                              time=time_slice)
#     # ptype=1: rain, ptype=7: mixture of rain and snow
#     ds['rainfall'] = xr.where((ds.ptype == 1) | (ds.ptype == 7), ds.tp, 0)
    
#     # %% Check for negative and nan values for rain and wind speed
#     neg_count = (ds < 0).sum(dim=['time', 'latitude', 'longitude'])
#     nan_count = ds.isnull().sum(dim=['time', 'latitude', 'longitude'])
#     # ds = ds.where(ds >0, 0)
    
#     # %% Calculate the damgae increments with apply_ufunc
#     """If the selected area is very big this step needs to be done in loops
#     with smaller chunks of data"""
#     test0, test1 = xr.apply_ufunc(impinged_rain, # the function
#                                   ds['ws'], ds['rainfall'], dt, 'IEA_15MW', #args
#                                   input_core_dims=[['time'], ['time'], [],[]],
#                                   output_core_dims=[['time'], ['time']],
#                                   vectorize=True,
#                                   dask='allowed')
    
#     # %%
#     ds['impinged_rain'] = test0.transpose('time', 'latitude', 'longitude')
#     ds['damage_inc'] = test1.transpose('time', 'latitude', 'longitude')
    
#     # %%
#     # delayed_obj = ds.to_netcdf('damage_increments.nc', compute=False)
#     # with ProgressBar():
#     #     results = delayed_obj.compute()
    
#     # %%
#     date_diff = ds.time[-1].values - ds.time[0].values
#     nr_years = date_diff / np.timedelta64(1, 'D')/365
#     nr_years = round(nr_years, 1)
    
#     # %% Calculate atlas layers 
#     al = xr.Dataset()
#     al['annual_rainfall'] = ds.rainfall.sum(dim='time')/nr_years
#     al['annual_impinged_rainfall'] = ds.impinged_rain.sum(dim='time')/nr_years
#     al['erosion_time'] = nr_years/ds.damage_inc.sum(dim='time')
#     al['wsp_150m'] = ds.ws.mean(dim='time')
#     al['annual_increment_sum'] = ds.damage_inc.sum(dim='time')/nr_years
    
#     # %%
#     delayed_obj = al.to_netcdf('atlas_layers.nc', compute=False)
#     with ProgressBar():
#         results = delayed_obj.compute()
# except:    
# %% Read in the NORA3 data
if start_at<=1:
<<<<<<< HEAD
warnings.simplefilter(action='ignore', category=FutureWarning)

try:
    print('attempting to load NORA3_ws_rr.nc')
    DS=xr.open_dataset(file_path +"NORA3_ws_rr.nc")
except:
      
    for it in range(0,len(t)):
        d=t[it]
        # get correct time for forecast and corresponding file name
        hb=(np.floor((d.hour+20)/6)*6).astype(int)
        hf=d.hour-hb+24
        if hb>24: 
            hb=hb-24
        df=d-pd.Timedelta(hours=hf)
        file = ('NORA3_wind_rain_subdomain_' + str(df.year) + str(df.month).zfill(2) + 
                  str(df.day).zfill(2) + str(df.hour).zfill(2) + '_' + str(hf).zfill(3) +
                  '_fp.nc')
        
        # read file
        print("reading: ",file)
        # with warnings.catch_warnings():
        #     warnings.simplefilter('ignore')
        ds = xr.open_dataset(file_path + str(df.year) +'/'+ file,drop_variables=dv
             ).sel(height2=height_slice,y=y_slice)#,x=x_slice)
        
        # preallocate dataset
        if it==0:
            print("preallocating dataset ", len(t),' x ',xl,' x ',yl) 
            DS = xr.Dataset(data_vars=dict(
                ws=(dims_3d, np.full((len(t),yl,xl), np.nan)),
                # tp=(dims_3d, np.full((len(t),yl,xl), np.nan)),
                rr=(dims_3d, np.full((len(t),yl,xl), np.nan)),
                impinged_rain=(dims_3d, np.full((len(t),yl,xl), np.nan)),
                damage_inc=(dims_3d, np.full((len(t),yl,xl), np.nan)),
                forecast=('time',np.full((len(t)),np.nan)),
                ),       
                coords=dict(
                    time=t,
                    y=ds.y.data,
                    x=ds.x.data,
                    lat=(["y","x"],ds.latitude.data),
                    lon=(["y","x"],ds.longitude.data),
                ),
                attrs=dict(description=""),
                )
            
            # plt.plot(
            #     DS.lon[0,0].data,DS.lat[0,0].data,'*',DS.lon[0,-1].data,DS.lat[0,-1].data,'*',
            #     DS.lon[-1,-1].data,DS.lat[-1,-1].data,'*',DS.lon[-1,0].data,DS.lat[-1,0].data,'*',
            #     DS.lon,DS.lat,
            #     # DS.lon[range(207,1098),range(0,652)],DS.lat[range(207,1098),range(0,652)],
            #     [0,0,32,32,0],[52,73,73,52,52],'.')
            # # plt.xlim(31,33)
            # # plt.ylim(72,74)
        
        # get precip reference data for every new forecast initialization (so when hf==4)
        if hf==4 or it==0:
            # read ref-file (3-hour forecast)
            ref_file = file[:-7] + str(hf-1) + file[-6:]
            # with warnings.catch_warnings():
                # warnings.simplefilter('ignore')
            ds_ref = xr.open_dataset(file_path + str(df.year) +'/'+ ref_file,
                     drop_variables=dvr).sel(y=y_slice)#,x=x_slice)
            ap0=ds_ref.precipitation_amount_acc
           
      
        # get wind and rain
        ws=np.absolute(ds.x_wind_z.interp(height2=hub_height) + 
                          1j*ds.y_wind_z.interp(height2=hub_height)
                          )
        DS.ws[it,:,:]=ws[0,:,:]
        # tp=ds.rainfall_amount
        # DS.tp[it,:,:] = tp[0,0,:,:]
            # https://thredds.met.no/thredds/projects/nora3.html
        ap=ds.precipitation_amount_acc
        
            # compute hourly rain rate from accumulated rain (see https://thredds.met.no/thredds/projects/nora3.html)
        DS.rr[it,:,:] = ap[0,0,:,:]-ap0[0,0,:,:]
        DS.forecast[it]=hf
        
        # store reference accumulated precipitation from previous forecast
        if hf==9:
            ap0=[]
        else:
            ap0=ap
        # save to file
        
    DS.to_netcdf(file_path+"NORA3_ws_rr.nc")
=======
    warnings.simplefilter(action='ignore', category=FutureWarning)

    for ti in range(0,len(ty)):
        t=ty[ti]
    # for yi in range(0,892,dy):
    #     y_r=range(yi,yi+dy)
    #     y_subslice = slice(N3_ygrid[yi]-1,N3_ygrid[yi+dy-1]+1) # corresponding to y-indeces(207,1098)

    #     for xi in range(0,652,dx):
    #         x_r=range(xi,xi+dx)
    #         x_subslice = slice(N3_xgrid[xi]-1,N3_xgrid[xi+dx-1]+1)

            # print('y-'+str(y_r)+' x-'+str(x_r))
        sfile=("NORA3_ws_precip_" +  str(t[0].year) + ".nc")
            # str(x_r[0])+"-"+str(x_r[-1])+"_y"+str(y_r[0])+"-"+str(y_r[-1])+ ".nc")
           
        try:
            print('attempting to load ' + sfile)
            DS=xr.open_dataset(file_path + sfile)
        except:
              
            for it in range(0,len(t)):
                d=t[it]
                # get correct time for forecast and corresponding file name
                hb=(np.floor((d.hour+20)/6)*6).astype(int)
                hf=d.hour-hb+24
                if hb>24: 
                    hb=hb-24
                df=d-pd.Timedelta(hours=hf)
                if use_local:
                    file = ('NORA3_wind_rain_subdomain_' + str(df.year) + str(df.month).zfill(2) + 
                              str(df.day).zfill(2) + str(df.hour).zfill(2) + '_' + str(hf).zfill(3) +
                              '_fp.nc')
                else:
                    file = (#str(df.year) +'/'+ str(df.month).zfill(2)+'/'+
                        #str(df.day).zfill(2)+'/'+ str(df.hour).zfill(2) + '/' +
                        'fc' + str(df.year).zfill(2) + str(df.month).zfill(2)+
                        str(df.day).zfill(2) + str(df.hour).zfill(2) + '_' + str(hf).zfill(3) +
                        '_fp.nc')
                
    
                # read file
                print("reading: ",file)
                # with warnings.catch_warnings():
                #     warnings.simplefilter('ignore')
                if use_local:
                    ds = xr.open_dataset(file_path + str(df.year) +'/'+ file,drop_variables=dv
                         ).sel(height2=height_slice,y=y_slice, x=x_slice)
                else:
                    ds = xr.open_dataset(remote_file_path + str(df.year) +'/'+ str(df.month).zfill(2)+'/'+
                        str(df.day).zfill(2)+'/'+ str(df.hour).zfill(2) + '/' + file,  # only for validation of rr
                        drop_variables=dv).sel(height2=height_slice, y=y_slice, x=x_slice)
                
                # preallocate dataset
                if it==0:
                    print("preallocating dataset ", len(t), ' * ', xl, ' * ', yl)
                    DS = xr.Dataset(data_vars=dict(
                        ws=(dims_3d, np.full((len(t),yl,xl), np.nan)), 
                        # rainrate_i=(dims_3d, np.full((len(t), yl, xl), np.nan)),
                        rainrate=(dims_3d, np.full((len(t),yl,xl), np.nan)),
                        # snowfallflux_i=(dims_3d, np.full((len(t), yl, xl), np.nan)),
                        # graupelfallflux_i=(dims_3d, np.full((len(t), yl, xl), np.nan)),
                        snowfallflux=(dims_3d, np.full((len(t), yl, xl), np.nan)),
                        graupelfallflux=(dims_3d, np.full((len(t), yl, xl), np.nan)),
                        haildiag=(dims_3d, np.full((len(t), yl, xl), np.nan)),
                        impinged_rain=(dims_3d, np.full((len(t),yl,xl), np.nan)),
                        damage_inc=(dims_3d, np.full((len(t),yl,xl), np.nan)),
                        forecast=('time',np.full((len(t)),np.nan)),
                        ),       
                        coords=dict(
                            time=t,
                            y=ds.y.data,
                            x=ds.x.data,
                            lat=(["y","x"],ds.latitude.data),
                            lon=(["y","x"],ds.longitude.data),
                        ),
                        attrs=dict(description=""),
                        )
                    
                    # plt.plot(
                    #     DS.lon[0,0].data,DS.lat[0,0].data,'*',DS.lon[0,-1].data,DS.lat[0,-1].data,'*',
                    #     DS.lon[-1,-1].data,DS.lat[-1,-1].data,'*',DS.lon[-1,0].data,DS.lat[-1,0].data,'*',
                    #     DS.lon,DS.lat,
                    #     # DS.lon[range(207,1098),range(0,652)],DS.lat[range(207,1098),range(0,652)],
                    #     [0,0,32,32,0],[52,73,73,52,52],'.')
                    # # plt.xlim(31,33)
                    # # plt.ylim(72,74)
                
                # get precip reference data for every new forecast initialization (so when hf==4)
                if hf==4 or it==0:
                    # read ref-file (3-hour forecast)
                    ref_file = file[:-7] + str(hf-1) + file[-6:]
                    print('read reference file '+ref_file)
                    # with warnings.catch_warnings():
                        # warnings.simplefilter('ignore')
                    if use_local:
                        ds_ref = xr.open_dataset(file_path + str(df.year) + '/' + ref_file,
                                            drop_variables=dvr).sel(y=y_slice)  # ,x=x_slice)
                    else:
                        ds_ref = xr.open_dataset(remote_file_path + str(df.year) +'/'+ str(df.month).zfill(2)+'/'+
                            str(df.day).zfill(2)+'/'+ str(df.hour).zfill(2) + '/' + ref_file,  # only for validation of rr
                            drop_variables=dvr).sel(height2=height_slice, y=y_slice, x=x_slice)
                    paa0 = ds_ref.precipitation_amount_acc
                    saa0 = ds_ref.snowfall_amount_acc
                    gaa0 = ds_ref.graupelfall_amount_acc
              
                # get wind and rain
                ws=np.absolute(ds.x_wind_z.interp(height2=hub_height) + 
                                  1j*ds.y_wind_z.interp(height2=hub_height)
                                  )
                DS.ws[it,:,:]=ws[0,:,:]
                # rri = ds.rainfall_amount  # only for validation of rr
                # DS.rainrate_i[it,:,:] = rri[0,0,:,:]*3600
                # sfi = ds.snowfall_amount  # only for validation of rr
                # DS.snowfallflux_i[it,:,:] = sfi[0,0,:,:]*3600
                # gfi = ds.graupelfall_amount  # only for validation of rr
                # DS.graupelfallflux_i[it,:,:] = gfi[0,0,:,:]*3600
                hd = ds.hail_diagnostic 
                DS.haildiag[it,:,:] = hd[0,0,:,:]
                
                # https://thredds.met.no/thredds/projects/nora3.html
                paa = ds.precipitation_amount_acc # includes also snow and graupel
                saa = ds.snowfall_amount_acc # includes also graupel
                gaa = ds.graupelfall_amount_acc # possibly includes hail 
                
                # compute hourly rates from accumulated values 
                #(current value - previous hour value, see https://thredds.met.no/thredds/projects/nora3.html)
                pr = paa[0,0,:,:]-paa0[0,0,:,:]
                sgr = saa[0, 0, :, :]-saa0[0, 0, :, :] # still including graupel
                gr = gaa[0, 0, :, :]-gaa0[0, 0, :, :] # potentially still including hail
                
                DS.rainrate[it,:,:] = pr - sgr
                DS.snowfallflux[it, :, :] = sgr-gr
                DS.graupelfallflux[it, :, :] = gr#*(1-hd[0,0,:,:])
                
                
                # store reference accumulated precipitation from previous forecast
                if hf==9:
                    paa0=[]
                    saa0=[]
                    gaa0=[]
                else:
                    paa0=paa
                    saa0=saa
                    gaa0=gaa
                # save to file
                
            DS.to_netcdf(file_path+sfile)
            
            
            # ax=plt.gca()
            # ax.plot(DS.time,DS.rainrate.mean(dim=['x','y']),
            #  # DS.time,DS.rainrate_i.mean(dim=['x','y']),
            #  DS.time,DS.snowfallflux.mean(dim=['x','y']),
            #  # DS.time,DS.snowfallflux_i.mean(dim=['x','y']),
            #  DS.time,DS.graupelfallflux.mean(dim=['x','y']),
            #  # DS.time,DS.graupelfallflux_i.mean(dim=['x','y']),
            #  )
            # ax.legend(['rain (acc)','rain (inst)','snow (acc)',
            #    'snow (inst)','graupel (acc)','graupel (inst)'])
            # ax.set_ylim(0,.005)
>>>>>>> 88d16047a4a3c30a91ec0669dbb8f68154e2826e
# %% Check for negative and nan values for rain and wind speed
# neg_count = (DS < 0).sum(dim=['time', 'y', 'x'])
# nan_count = DS.isnull().sum(dim=['time', 'y', 'x'])
# # ds = ds.where(ds >0, 0)

# %% Calculate the damage increments with apply_ufunc
if start_at<=2:
<<<<<<< HEAD
with warnings.catch_warnings():
    warnings.simplefilter('ignore')

    dy=223 # 1,2,4,223,446,892
    dx=163 # 1,2,4,163,326,652
#    for yi in range(0,4,dy):
    for yi in range(0,892,dy):
        y_r=range(yi,yi+dy)
        for xi in range(0,652,dx):
            x_r=range(xi,xi+dx)
            print('y-'+str(y_r)+' x-'+str(x_r))
            ds=xr.Dataset()
            ds['ws']=DS.ws[:,y_r,x_r]
            ds['rr']=DS.rr[:,y_r,x_r]
            ds['impinged_rain']=DS.impinged_rain[:,y_r,x_r]
            ds['damage_inc']=DS.damage_inc[:,y_r,x_r]

            te=time.time()
            imp_r, dam_inc = xr.apply_ufunc(impinged_rain, # the function
#                  DS['ws'][:,y_r,x_r], DS['rr'][:,y_r,x_r]/1000, dt, 'IEA_15MW', #args
                  ds['ws'][:,range(0,dy),range(0,dx)], DS['rr'][:,y_r,x_r]/1000, dt, 'IEA_15MW', #args
=======
    with warnings.catch_warnings():
        warnings.simplefilter('ignore')
    
    #    for yi in range(0,4,dy):
        for ti in range(0,len(ty)):
            t=ty[ti]

        # for yi in range(0,892,dy):
        #     y_r=range(yi,yi+dy)
        #     for xi in range(0,652,dx):
        #         x_r=range(xi,xi+dx)
        #         print('compute impingement and damage for y-'+str(y_r)+' x-'+str(x_r))
                
            # read data
            sfile=("NORA3_ws_precip_" +  str(t[0].year) + ".nc")
            print('loading '+sfile)
            ds=xr.open_dataset(file_path+sfile)#drop_variables=['snowfallflux','graupelfallflux','haildiag','forecast'])
            
            # ds=xr.Dataset()
            # ds['ws']=DS.ws#[:,y_r,x_r]
            # ds['rainrate']=DS.rainrate#[:,y_r,x_r]
            # # ds['rainrate_i']=DS.rainrate_i[:,y_r,x_r]
            # # ds['snowfallflux_i']=DS.snowfallflux_i[:,y_r,x_r]
            # # ds['snowfallflux']=DS.snowfallflux[:,y_r,x_r]
            # # ds['graupelfallflux_i']=DS.graupelfallflux_i[:,y_r,x_r]
            # # ds['graupelfallflux']=DS.graupelfallflux[:,y_r,x_r]
            # ds['impinged_rain']=DS.impinged_rain[:,y_r,x_r]
            # ds['damage_inc']=DS.damage_inc[:,y_r,x_r]

            print('calculating impinged rain and damage increment')
            te=time.time()
            imp_r, dam_inc = xr.apply_ufunc(impinged_rain, # the function
#                  DS['ws'][:,y_r,x_r], DS['rr'][:,y_r,x_r]/1000, dt, 'IEA_15MW', #args
                  ds['ws'], 
                  ds['rainrate']/1000, dt, 
                  'IEA_15MW', #args
>>>>>>> 88d16047a4a3c30a91ec0669dbb8f68154e2826e
                  # DS['ws'][it,:,:], DS['tp'][it,:,:], dt, 'IEA_15MW', #args
                  input_core_dims=[['time'], ['time'], [],[]],
                  output_core_dims=[['time'], ['time']],
                  # input_core_dims=[DS['time'], DS['time'], [],[]],
                  # output_core_dims=[DS['time'], DS['time']],
                  vectorize=True,
                  dask='allowed')
            te=time.time()-te
<<<<<<< HEAD
            
            #ds['impinged_rain'][:,y_r,x_r] = imp_r.transpose('time', 'y', 'x')
            #ds['damage_inc'][:,y_r,x_r] = dam_inc.transpose('time', 'y', 'x')
            ds['impinged_rain'] = imp_r.transpose('time', 'y', 'x')
            ds['damage_inc'] = dam_inc.transpose('time', 'y', 'x')
            print('elapsed time = '+str(te) +' sec')
            
#            save to nc
            print('saving nc file')
            ds.to_netcdf(file_path+"NORA3_ws_rr_imp/NORA3_ws_rr_imp_x"+str(x_r[0])+"-"+str(x_r[-1])+"_y"+str(y_r[0])+"-"+str(y_r[-1])+".nc")

=======
            
            #ds['impinged_rain'][:,y_r,x_r] = imp_r.transpose('time', 'y', 'x')
            #ds['damage_inc'][:,y_r,x_r] = dam_inc.transpose('time', 'y', 'x')
            ds['impinged_rain'] = imp_r.transpose('time', 'y', 'x')
            ds['damage_inc'] = dam_inc.transpose('time', 'y', 'x')
            print('elapsed time = '+str(te) +' sec')
            
#            save to nc
            print('saving nc file')
            # tmpfile=("NORA3_ws_precip_tmp.nc")
            # ds.to_netcdf(file_path+tmpfile,mode='w')
            # os.rename(file_path+tmpfile,file_path+sfile)
            ds.to_netcdf(file_path+sfile[0:15]+"_imp"+sfile[15:len(sfile)])
>>>>>>> 88d16047a4a3c30a91ec0669dbb8f68154e2826e
        
# %%
#a1=plt.axes()
#a1.pcolor(DS.impinged_rain[0,:,:].transpose('x','y'))
#plt.plot()

# %% POI: 
if start_at<=3:
# POI = {"name": ["Utsira", "Bergen", "Thyboron", "Anholt", "Bornholm", "Horns Rev 2", 
#                 "Kriegers Flak", "Rodsand II", "Thor", "Vindeby-Lolland", 
#                 "Alpha Ventus", "Dan Tysk", "EnBW Baltic 1", "Hywind Tampen", 
#                 "Sorlige Nordsjo II"],
#        "lat": [59.30693, 60.38373, 56.706792, 56.600, 54.980, 55.600, 
#                55.030, 54.558, 56.330, 54.970,
#                54.017, 55.140, 54.609, 61.330, 56.780 ],
#        "lon": [4.87135, 5.33197, 8.214954, 11.210, 14.740, 7.582, 
#                12.940, 11.531, 7.640, 11.130, 
#                6.600, 7.200, 12.651, 2.700, 4.920]
#        }
# POI["lat"][1]

POI = {"Utsira": [59.30693, 4.87135],
       "Bergen Florida": [60.38373, 5.33197],
       "Thyboroen": [56.706792, 8.214954],
       "Anholt":[56.600,	11.210],	
       "Bornholm":	[54.980,	14.740],
        "Horns Rev 2	":[55.600,7.582],
        "Kriegers Flak":[55.030,12.940],
        "Rodsand II":[54.558,11.531],
        "Thor":[56.330,7.640],
        "Vindeby-Lolland":[54.970,11.130],
        "Alpha Ventus":[54.017,6.600],
        "Dan Tysk":[55.140,7.200],
        "EnBW Baltic 1":[54.609,12.651],
        "Hywind Tampen":[61.330,2.700],
        "Sorlige Nordsjo II":[56.780,4.920]
        }
#Nordvest A						
#	Nordvest C						
#	Vestavind B						
#	Sønnavind A						

dims_poi=('time','poi')
DS_poi=xr.Dataset(data_vars=dict(
    ws=(dims_poi, np.full((len(DS.time),len(POI)), np.nan)),
    rr=(dims_poi, np.full((len(DS.time),len(POI)), np.nan)),
    impinged_rain=(dims_poi, np.full((len(DS.time),len(POI)), np.nan)),
    damage_inc=(dims_poi, np.full((len(DS.time),len(POI)), np.nan)),
    forecast=('time',DS.forecast),
    ),       
    coords=dict(
        time=DS.time,
        poi=("poi",range(0,len(POI))),
        POI=("poi",list(POI.keys())),#np.full(len(POI),"                  ")),
        lat=("poi",np.full(len(POI),np.nan)),
        lon=("poi",np.full(len(POI),np.nan)),
        lat0=("poi",np.array(list(POI.values()))[:,0]),
        lon0=("poi",np.array(list(POI.values()))[:,1]),
    ),
    attrs=dict(description=""),
    )

c=0
for p in POI:
    iyx=np.unravel_index(np.argmin(np.abs((POI[p][0]-DS.lat)+1j*(POI[p][1]-DS.lon)),axis=None), DS.lat.shape)
    print(iyx, p, POI[p], DS.lat[iyx].values,  DS.lon[iyx].values)
    
<<<<<<< HEAD
    
    # find correct file
    dy=223 # 1,2,4,223,446,892
    dx=163 # 1,2,4,163,326,652
    ix=int(np.floor(iyx[1]/dx)*dx)
    iy=int(np.floor(iyx[0]/dy)*dy)
    x_r=[ix,ix+dx-1]
    y_r=[iy,iy+dy-1]
    ds=xr.open_dataset(file_path+"NORA3_ws_rr_imp/NORA3_ws_rr_imp_x"+str(x_r[0])+"-"+
                       str(x_r[-1])+"_y"+str(y_r[0])+"-"+str(y_r[-1])+".nc").sel(x=DS.x[iyx[1]],y=DS.y[iyx[0]])
    
    DS_poi.lat[c]=ds.lat
    DS_poi.lon[c]=ds.lon
    DS_poi.ws[:,c]=ds.ws    
    DS_poi.rr[:,c]=ds.rr
    DS_poi.impinged_rain[:,c]=ds.impinged_rain
    DS_poi.damage_inc[:,c]=ds.damage_inc
    
    c=c+1
    
print('saving nc file')
DS_poi.to_netcdf(file_path+"NORA3_ws_rr_imp/NORA3_POIs.nc", mode="w")

#%% plots POI
if start_at<=4:
if 'DS_poi' not in vars():
    DS_poi=xr.open_dataset(file_path +"NORA3_ws_rr_imp/NORA3_POIs.nc")

plt.plot(DS_poi.time,np.cumsum(DS_poi.rr/1000))
plt.plot(DS_poi.time,np.cumsum(DS_poi.impinged_rain))
plt.plot(DS_poi.time,np.cumsum(DS_poi.damage_inc))

plt.hist(DS_poi.ws), plt.legend(DS_poi.POI.values())

#%% correlation
R_rr_ir=np.empty(len(DS_poi.poi))
R_di_ir=np.copy(R_rr_ir)
R_rr_di=np.copy(R_rr_ir)
R_ws_ir=np.empty(len(DS_poi.poi))
R_ws_di=np.copy(R_rr_ir)
for p in DS_poi.poi:
    R=np.corrcoef(DS_poi.rr[:,p], DS_poi.impinged_rain[:,p])
    R_rr_ir[p]=R[0,1]
    R=np.corrcoef(DS_poi.rr[:,p], DS_poi.damage_inc[:,p])
    R_rr_di[p]=R[0,1]
    R=np.corrcoef(DS_poi.damage_inc[:,p], DS_poi.impinged_rain[:,p])
    R_di_ir[p]=R[0,1]
    R=np.corrcoef(DS_poi.ws[:,p], DS_poi.impinged_rain[:,p])
    R_ws_ir[p]=R[0,1]
    R=np.corrcoef(DS_poi.ws[:,p], DS_poi.damage_inc[:,p])
    R_ws_di[p]=R[0,1]
    
#%% more plots
fig, ax = plt.subplots()
c=ax.pcolor(DS.x, DS.y, np.sum(DS.rr,0)/5000)#.transpose('x','y')
fig.colorbar(c,ax=ax)
ax.set_title('mean anual accumulated precipitation [m]')
=======
    # get grid information
    DS=xr.open_dataset(file_path +"NORA3_ws_precip_2015.nc",drop_variables=[
        'time',
        'ws','rainrate','impinged_rain','damage_inc','forecast','graupelfallflux',
        'snowfallflux','haildiag'])

    
    POI = {"Utsira": [59.30693, 4.87135],
            "Bergen Florida": [60.38373, 5.33197],
            "Thyboroen": [56.706792, 8.214954],
            "Anholt":[56.600,	11.210],	
            "Bornholm":	[54.980,	14.740],
            "Horns Rev 2	":[55.600,7.582],
            "Kriegers Flak":[55.030,12.940],
            "Rodsand II":[54.558,11.531],
            "Thor":[56.330,7.640],
            "Vindeby-Lolland":[54.970,11.130],
            "Alpha Ventus":[54.017,6.600],
            "Dan Tysk":[55.140,7.200],
            "EnBW Baltic 1":[54.609,12.651],
            "Hywind Tampen":[61.330,2.700],
            "Sorlige Nordsjo II":[56.780,4.920]
}
    #Nordvest A						
    #	Nordvest C						
    #	Vestavind B						
    #	Sønnavind A						
    
    dims_poi=('time','poi')
    DS_poi=xr.Dataset(data_vars=dict(
        ws=(dims_poi, np.full((tl,len(POI)), np.nan)),
        rainrate=(dims_poi, np.full((tl,len(POI)), np.nan)),
        snowfallflux=(dims_poi, np.full((tl,len(POI)),np.nan)),
        graupelfallflux=(dims_poi, np.full((tl,len(POI)),np.nan)),
        impinged_rain=(dims_poi, np.full((tl,len(POI)), np.nan)),
        damage_inc=(dims_poi, np.full((tl,len(POI)), np.nan)),
        forecast=('time',np.full(tl,np.nan)),
        ),       
        coords=dict(
            time=("time",np.full(tl,pd.date_range(start=ty[0][0], end=ty[-1][-1],freq='H'))),
            poi=("poi",range(0,len(POI))),
            POI=("poi",list(POI.keys())),#np.full(len(POI),"                  ")),
            lat=("poi",np.full(len(POI),np.nan)),
            lon=("poi",np.full(len(POI),np.nan)),
            lat0=("poi",np.array(list(POI.values()))[:,0]),
            lon0=("poi",np.array(list(POI.values()))[:,1]),
        ),
        attrs=dict(description=""),
        )
    
    c=0
    for p in POI:
        # find representative grid point
        d_deg=np.abs((POI[p][0]-DS.lat)+1j*(POI[p][1]-DS.lon))
        iyx=np.unravel_index(np.argmin(d_deg.data,axis=None), DS.lat.shape)
        x=DS.x[iyx[1]]
        y=DS.y[iyx[0]]
        print(iyx, p, POI[p], DS.lat[iyx].values,  DS.lon[iyx].values)
       
        # find correct file of subdomains
        # dy=223 # 1,2,4,223,446,892
        # dx=163 # 1,2,4,163,326,652
        # ix=int(np.floor(iyx[1]/dx)*dx)
        # iy=int(np.floor(iyx[0]/dy)*dy)
        # x_r=[ix,ix+dx-1]
        # y_r=[iy,iy+dy-1]
        # ds=xr.open_dataset(file_path+"NORA3_ws_rr_imp/NORA3_ws_precip_imp_x"+str(x_r[0])+"-"+
        #                    str(x_r[-1])+"_y"+str(y_r[0])+"-"+str(y_r[-1])+".nc").sel(x=DS.x[iyx[1]],y=DS.y[iyx[0]])

        # loop through yearly files
        it0=0
        ds_poi=[]
        for ti in range(0,5):
            t=ty[ti]
            sfile=("NORA3_ws_precip_imp_" +  str(t[0].year) + ".nc")
            print('reading '+sfile)
            ds=xr.open_dataset(file_path+sfile).sel(x=x,y=y)
            tr=range(it0,it0+len(ds.time))
            it0=tr.stop
            DS_poi.ws[tr,c]=ds.ws    
            DS_poi.rainrate[tr,c]=ds.rainrate
            DS_poi.snowfallflux[tr,c]=ds.snowfallflux
            DS_poi.graupelfallflux[tr,c]=ds.graupelfallflux
            DS_poi.impinged_rain[tr,c]=ds.impinged_rain
            DS_poi.damage_inc[tr,c]=ds.damage_inc
            if c==0:
                DS_poi.forecast[tr]=ds.forecast
            if ti==0:
                DS_poi.lat[c]=ds.lat
                DS_poi.lon[c]=ds.lon
        c=c+1
        
    print('saving nc file')
    DS_poi.to_netcdf(file_path+"NORA3_ws_rr_imp/NORA3_POIs.nc", mode="w")

# #%% plots POI
# if start_at<=4:
#     if 'DS_poi' not in vars():
#         DS_poi=xr.open_dataset(file_path +"NORA3_ws_rr_imp/NORA3_POIs.nc")
    
#     plt.plot(DS_poi.time,np.cumsum(DS_poi.rr/1000))
#     plt.plot(DS_poi.time,np.cumsum(DS_poi.impinged_rain))
#     plt.plot(DS_poi.time,np.cumsum(DS_poi.damage_inc))
    
#     plt.hist(DS_poi.ws), plt.legend(DS_poi.POI.values())
    
#     #%% correlation
#     R_rr_ir=np.empty(len(DS_poi.poi))
#     R_di_ir=np.copy(R_rr_ir)
#     R_rr_di=np.copy(R_rr_ir)
#     R_ws_ir=np.empty(len(DS_poi.poi))
#     R_ws_di=np.copy(R_rr_ir)
#     for p in DS_poi.poi:
#         R=np.corrcoef(DS_poi.rr[:,p], DS_poi.impinged_rain[:,p])
#         R_rr_ir[p]=R[0,1]
#         R=np.corrcoef(DS_poi.rr[:,p], DS_poi.damage_inc[:,p])
#         R_rr_di[p]=R[0,1]
#         R=np.corrcoef(DS_poi.damage_inc[:,p], DS_poi.impinged_rain[:,p])
#         R_di_ir[p]=R[0,1]
#         R=np.corrcoef(DS_poi.ws[:,p], DS_poi.impinged_rain[:,p])
#         R_ws_ir[p]=R[0,1]
#         R=np.corrcoef(DS_poi.ws[:,p], DS_poi.damage_inc[:,p])
#         R_ws_di[p]=R[0,1]
        
#     #%% more plots
#     fig, ax = plt.subplots()
#     c=ax.pcolor(DS.x, DS.y, np.sum(DS.rr,0)/5000)#.transpose('x','y')
#     fig.colorbar(c,ax=ax)
#     ax.set_title('mean anual accumulated precipitation [m]')
>>>>>>> 88d16047a4a3c30a91ec0669dbb8f68154e2826e
    
#%% mean values for entire domain
if start_at<=5:
    print('computing statistics')
    # # increments for subdomains
    # dy=223 # 1,2,4,223,446,892
    # dx=163 # 1,2,4,163,326,652
    # open original file
    DS=xr.open_dataset(file_path +"NORA3_ws_precip_imp_2015.nc", drop_variables=
       ['ws','rainrate','snowfallflux','graupelfallflux','haildiag',
        'impinged_rain','damage_inc','forecast'])
    nyears=int(np.round(tl/365/24))
    print(nyears)
    # create data file for statistics
    DS_am=xr.Dataset(data_vars=dict(
        mean_ws=(dims_2d, np.full((yl,xl), 0)),
        annual_accumulated_rain=(dims_2d, np.full((yl,xl), 0)),
        annual_accumulated_impinged_rain=(dims_2d, np.full((yl,xl), 0)),
        annual_accumulated_damage_inc=(dims_2d, np.full((yl,xl), 0)),
        ),
        coords=dict(
            y=DS.y,
            x=DS.x,
            lat=(["y","x"],DS.lat.data),
            lon=(["y","x"],DS.lon.data),
            ),
        attrs=dict(description=""),
        )
    # loop through subdomain files
<<<<<<< HEAD
    for yi in range(0,892,dy):
        y_r=range(yi,yi+dy)
        for xi in range(0,652,dx):
            x_r=range(xi,xi+dx)
            # open subdomain file
            ds=xr.open_dataset(file_path + "NORA3_ws_rr_imp/NORA3_ws_rr_imp_x" +
                str(x_r[0]) + "-" + str(x_r[-1]) + "_y" + str(y_r[0]) + "-" + str(y_r[-1]) + ".nc")
            # caclulate annual statistics
            print('computing mean ws for subdomain: y-'+str(y_r)+' x-'+str(x_r))
            DS_am.mean_ws[y_r,x_r]=np.mean(ds.ws,0)
            print('computing accumulated rain for subdomain: y-'+str(y_r)+' x-'+str(x_r))
            DS_am.annual_accumulated_rain[y_r,x_r]=np.sum(ds.rr,0)/nyears # in mm
            print('computing accumulated impinged rain for subdomain: y-'+str(y_r)+' x-'+str(x_r))
            DS_am.annual_accumulated_impinged_rain[y_r,x_r]=np.sum(ds.imp,0)/nyears # in mm
            print('computing accumulated damage_inc for subdomain: y-'+str(y_r)+' x-'+str(x_r))
            DS_am.annual_accumulated_damage_inc[y_r,x_r]=np.sum(ds.damage_inc,0)/nyears

=======
    # for yi in range(0,892,dy):
    #     y_r=range(yi,yi+dy)
    #     for xi in range(0,652,dx):
    #         x_r=range(xi,xi+dx)
    #         # open subdomain file
    #         ds=xr.open_dataset(file_path + "NORA3_ws_rr_imp/NORA3_ws_precip_imp_x" +
    #             str(x_r[0]) + "-" + str(x_r[-1]) + "_y" + str(y_r[0]) + "-" + str(y_r[-1]) + ".nc")
    #         # caclulate annual statistics
    #         print('computing accumulated impinged rain for subdomain: y-'+str(y_r)+' x-'+str(x_r))
    #         DS_am.annual_accumulated_impinged_rain[y_r,x_r]=np.sum(ds.impinged_rain,0)/nyears # in mm
    #         print('computing accumulated damage_inc for subdomain: y-'+str(y_r)+' x-'+str(x_r))
    #         DS_am.annual_accumulated_damage_inc[y_r,x_r]=np.sum(ds.damage_inc,0)/nyears
    #         print('computing mean ws for subdomain: y-'+str(y_r)+' x-'+str(x_r))
    #         DS_am.mean_ws[y_r,x_r]=np.mean(ds.ws,0)
    #         print('computing accumulated rain for subdomain: y-'+str(y_r)+' x-'+str(x_r))
    #         DS_am.annual_accumulated_rain[y_r,x_r]=np.sum(ds.rainrate,0)/nyears # in mm
    
    for ti in range(0,nyears):
        t=ty[ti]
        sfile=("NORA3_ws_precip_imp_" +  str(t[0].year) + ".nc")
        print('reading '+sfile)
        ds=xr.open_dataset(file_path+sfile,drop_variables=['snowfallflux',
               'graupelfallflux','haildiag','forecast'])

        # caclulate annual statistics (year by year)
        print('computing accumulated impinged rain for '+str(t[0].year))
        DS_am['annual_accumulated_impinged_rain']=np.sum(ds.impinged_rain,0)/nyears+DS_am['annual_accumulated_impinged_rain']# in mm
        print('computing accumulated damage_inc for '+str(t[0].year))
        DS_am['annual_accumulated_damage_inc']=np.sum(ds.damage_inc,0)/nyears+DS_am['annual_accumulated_damage_inc']
        print('computing mean ws for '+str(t[0].year))
        DS_am['mean_ws']=np.sum(ds.ws,0)/tl+DS_am['mean_ws']
        print('computing accumulated rain for '+str(t[0].year))
        DS_am['annual_accumulated_rain']=np.sum(ds.rainrate,0)/nyears+DS_am['annual_accumulated_rain'] # in mm
            
>>>>>>> 88d16047a4a3c30a91ec0669dbb8f68154e2826e
    print('saving nc file')
    DS_am.to_netcdf(file_path+"NORA3_ws_rr_imp/NORA3_statistics.nc", mode="w")
