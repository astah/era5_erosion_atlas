# %%
import xarray as xr
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

ds = xr.open_dataset('atlas_layers.nc')

# %% Plot the annual rainfall
fig = plt.figure()
pcm = ds.annual_rainfall.plot(add_colorbar=False)
fig.colorbar(pcm, label='annual average rainfall [m]')
pcm.get_figure().gca().set_title('')
plt.savefig('plots/annual_rainfall.png', dpi=300)

# %% Annual impinged rainfall
fig = plt.figure()
pcm = ds.annual_impinged_rainfall.plot(add_colorbar=False)
fig.colorbar(pcm, label='annual impinged rainfall [m]')
pcm.get_figure().gca().set_title('')
plt.savefig('plots/annual_impinged_rainfall.png', dpi=300)

# %% Average erosion onset time
fig = plt.figure()
pcm = ds.erosion_time.plot(add_colorbar=False, vmax=20)
fig.colorbar(pcm, extend='max', label='erosion onset time [years]')
pcm.get_figure().gca().set_title('')
plt.savefig('plots/erosion_onset_time.png', dpi=300)

# %% Mean wind speed at 150 m
fig = plt.figure()
pcm = ds.wsp_150m.plot(add_colorbar=False)
fig.colorbar(pcm, label='mean wind speed at 150m [m/s]')
pcm.get_figure().gca().set_title('')
plt.savefig('plots/mean_wsp_150m.png', dpi=300)

# %% Annual average damge increment sum
fig = plt.figure()
pcm = ds.annual_increment_sum.plot(add_colorbar=False)
fig.colorbar(pcm, label='annual damage increment sum [-]')
pcm.get_figure().gca().set_title('')
plt.savefig('plots/annual_increment_sum.png', dpi=300)

# %%
